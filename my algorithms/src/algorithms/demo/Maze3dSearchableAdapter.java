package algorithms.demo;

import java.util.ArrayList;

import algorithms.mazeGenerators.Maze3d;
import algorithms.mazeGenerators.Position;
import algorithms.mazeGenerators.State;
import algorithms.searchable.Searchable;

/**
 * The Class Maze3dSearchableAdapter.
 * Object adapter.
 * performs adaptation from maze (an instance of Maze3d) to the problem of search (Searchable)
 */
public class Maze3dSearchableAdapter implements Searchable<Position>{

	/** The maze. */
	private Maze3d myMaze3d;

	/**
	 * Gets the my maze3d.
	 *
	 * @return the my maze3d
	 */
	public Maze3d getMyMaze3d() {
		return myMaze3d;
	}

	/**
	 * Sets the my maze3d.
	 *
	 * @param myMaze3d the new my maze3d
	 */
	public void setMyMaze3d(Maze3d myMaze3d) {
		this.myMaze3d = myMaze3d;
	}

	/**
	 * Instantiates a new maze3d searchable adapter.
	 *
	 * @param maze the maze
	 */
	public Maze3dSearchableAdapter(Maze3d maze) {
		this.setMyMaze3d(maze);
	}

	/* (non-Javadoc)
	 * @see algorithms.Searchable.Searchable#getStartPosition()
	 */
	@Override
	public State<Position> getStartState() {
		State<Position> start = new State<Position>(myMaze3d.getStartPosition(), null, 0);
		return start;
	}

	/* (non-Javadoc)
	 * @see algorithms.Searchable.Searchable#getGoalPosition()
	 */
	@Override
	public State<Position> getGoalState() {
		State<Position> goal = new State<Position>(myMaze3d.getGoalPosition(), null, 0);
		return goal;
	}

	/* (non-Javadoc)
	 * @see algorithms.Searchable.Searchable#getPossibleMoves(algorithms.mazeGenerators.State)
	 */
	@Override
	public ArrayList<State<Position>> getPossibleMoves(State<Position> currentState) {
		ArrayList<State<Position>> possMoves = new ArrayList<State<Position>>();
		ArrayList<Position> possMovesTemp = new ArrayList<Position>();
		possMovesTemp = myMaze3d.getPossibleMoves((Position) currentState.getCurrentState());

		for(int i=0; i<possMovesTemp.size();i++){
			possMoves.add(new State<Position>(possMovesTemp.get(i), currentState, 1));
		}

		return possMoves;
	}

}
