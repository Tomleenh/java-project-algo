package algorithms.demo;

import algorithms.mazeGenerators.Maze3d;
import algorithms.mazeGenerators.Maze3dGenerator;
import algorithms.mazeGenerators.MyMaze3dGenerator;
import algorithms.mazeGenerators.Position;
import algorithms.search.AStarAlgorithm;
import algorithms.search.BFSAlgorithm;
import algorithms.search.MazeAirDistance;
import algorithms.search.MazeManhattanDistance;
import algorithms.search.Searcher;
import algorithms.search.Solution;

/**
 * The Class Demo.
 * include the method run() to run the program
 */
public class Demo {

	/**
	 * The run method creates a three-dimensional maze consists of using MyMazeGenerator,
	 * Prints it, Solve it using BFS, AStar solve it by using any Heuristics, and prints the solution
	 * and prints the number of States each algorithm developed.
	 */
	public void run() {

		Maze3dGenerator maze3dGenerator = new MyMaze3dGenerator();
		Maze3d mg = maze3dGenerator.generate(5,9,7);
		Searcher<Position> searcher= new AStarAlgorithm<Position>(new MazeManhattanDistance());
		Searcher<Position> searcher2 = new AStarAlgorithm<Position>(new MazeAirDistance());
		Searcher<Position> searcher3 = new BFSAlgorithm<>();
		Solution<Position> solu = new Solution<Position>();
		Solution<Position> solu2 = new Solution<Position>();
		Solution<Position> solu3 = new Solution<Position>();
		solu = searcher.search(new Maze3dSearchableAdapter(mg));
		solu2 = searcher2.search(new Maze3dSearchableAdapter(mg));
		solu3 = searcher3.search(new Maze3dSearchableAdapter(mg));

		mg.printMaze();

		System.out.println("\n");
		System.out.println("Start position: " +mg.getStartPosition());
		System.out.println("Goal position: " + mg.getGoalPosition());
		System.out.println();

		System.out.println("*********************************************************************************");
		System.out.println("A* Algorithm - Manhattan Distance");
		System.out.println("Solution: " + solu.getSolution());
		System.out.println("Evaluted nodes: " + searcher.getNumberOfNodesEvaluated());
		System.out.println("*********************************************************************************");
		System.out.println();
		System.out.println("*********************************************************************************");
		System.out.println("A* Algorithm - Air Distance");
		System.out.println("Solution: " + solu2.getSolution());
		System.out.println("Evaluted nodes: " + searcher2.getNumberOfNodesEvaluated());
		System.out.println("*********************************************************************************");
		System.out.println();
		System.out.println("*********************************************************************************");
		System.out.println("BFS Algorithm");
		System.out.println("Solution: " + solu3.getSolution());
		System.out.println("Evaluted nodes: " + searcher3.getNumberOfNodesEvaluated());
		System.out.println("*********************************************************************************");
	}
}
