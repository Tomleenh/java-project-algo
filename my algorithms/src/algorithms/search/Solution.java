package algorithms.search;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * The Class Solution.
 *
 * @param <T> the generic type
 */
@SuppressWarnings("serial")
public class Solution<T> implements Serializable{


	/** The solution. */
	private ArrayList<T> solution = new ArrayList<>();

	/**
	 * Instantiates a new solution.
	 *
	 * @param solution the solution
	 */
	public Solution(ArrayList<T> solution) {
		this.solution = solution;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		return this.solution.toString();
	}

	/**
	 * Instantiates a new solution.
	 */
	public Solution() {
		this.solution = null;
	}

	/**
	 * Gets the solution.
	 *
	 * @return the solution
	 */
	public ArrayList<T> getSolution() {
		return solution;
	}

	/**
	 * Sets the solution.
	 *
	 * @param solution the new solution
	 */
	public void setSolution(ArrayList<T> solution) {
		this.solution = solution;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((solution == null) ? 0 : solution.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Solution other = (Solution) obj;
		if (solution == null) {
			if (other.solution != null)
				return false;
		} else if (!solution.equals(other.solution))
			return false;
		return true;
	}

}
