package algorithms.search;

import algorithms.mazeGenerators.Position;
import algorithms.mazeGenerators.State;

/**
 * The Class MazeManhattanDistance.
 * Calculates the Manhattan distance : distance x + distance y + distance z
 */
public class MazeManhattanDistance implements Heuristic<Position> {

	/* (non-Javadoc)
	 * @see algorithms.search.Heuristic#heuristicalDistance(algorithms.mazeGenerators.State, algorithms.mazeGenerators.State)
	 */
	@Override
	public int heuristicDistance(State<Position> currentState,State<Position> goalState) {
		return (Math.abs(currentState.getCurrentState().getX()-goalState.getCurrentState().getX()) + 
				Math.abs(currentState.getCurrentState().getY()-goalState.getCurrentState().getY()) + 
				Math.abs(currentState.getCurrentState().getZ()-goalState.getCurrentState().getZ()));
	}
}
