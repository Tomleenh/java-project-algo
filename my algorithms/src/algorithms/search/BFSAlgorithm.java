package algorithms.search;

import java.util.ArrayList;
import java.util.HashMap;

import algorithms.mazeGenerators.State;
import algorithms.searchable.Searchable;

/**
 * The Class BFSAlgorithm.
 * The BFSAlgorithm class implements an best-first search algorithm.
 *
 * @param <T> the generic type
 */
public class BFSAlgorithm<T> extends CommonSearcher<T> {

	/* (non-Javadoc)
	 * @see algorithms.search.CommonSearcher#search(algorithms.searchable.Searchable)
	 */
	@Override
	public Solution<T> search(Searchable<T> s) {
		myHash = new HashMap<>();
		openList.add(s.getStartState());
		myHash.put(s.getStartState().getCurrentState(), 0);

		//the algorithm runs till the open list is zero or we reached the goal position
		while(!openList.isEmpty())
		{
			State<T> currentState = popOpenList(); //dequeue from Priority List
			closedSet.add(currentState);

			//if we reached the goal we are doing back trace through cameFrom
			if(currentState.getCurrentState().equals(s.getGoalState().getCurrentState()))
			{
				return generatePathToGoal(currentState);
			}

			//array list of possible positions to move to
			ArrayList<State<T>> successors = s.getPossibleMoves(currentState);
			for(State<T> neighbor : successors)
			{

				int newCost = neighbor.getCost()+currentState.getCost();
				int oldCost = newCost+1;
				if(myHash.containsKey(neighbor.getCurrentState())){
					oldCost = myHash.get(neighbor.getCurrentState());
				}

				//if the position we found isn't in the open list or the closed list meaning he's a new node we discovered
				if(!closedSet.contains(neighbor.getCurrentState()) && !openList.contains(neighbor.getCurrentState()) && (newCost < oldCost))
				{
					neighbor.setCost(newCost);
					neighbor.setCameFrom(currentState);
					openList.add(neighbor);
					myHash.put(neighbor.getCurrentState(), neighbor.getCost());
				}
				//if we find a neighbor that costs us less we update that neighbor
				else if (newCost < oldCost){
					myHash.remove(neighbor.getCurrentState());

					openList.remove(neighbor);
					neighbor.setCost(newCost);
					neighbor.setCameFrom(currentState);
					openList.add(neighbor);
					myHash.put(neighbor.getCurrentState(), newCost);

				}
			}

		}
		return null;
	}	


}

