package algorithms.search;

import algorithms.mazeGenerators.State;

/**
 * The Interface Heuristic.
 *
 * @param <T> the generic type
 */
public interface Heuristic<T> {

	/**
	 * Heuristic distance.
	 *
	 * @param currentState the current state
	 * @param goalState the goal state
	 * @return the distance as a number
	 */
	int heuristicDistance(State<T> currentState, State<T> goalState);
}
