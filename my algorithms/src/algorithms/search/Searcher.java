package algorithms.search;

import algorithms.searchable.Searchable;

/**
 * The Interface Searcher.
 *
 * @param <T> the generic type
 */
public interface Searcher<T> {

	/**
	 * Search.
	 * search a path from the start position to the goal using a search algorithm.
	 *
	 * @param s the searchable
	 * @return the solution of the search Algorithm
	 */
	//the search method
	Solution<T> search(Searchable<T> s);

	/**
	 * Gets the number of nodes evaluated by the search algorithm.
	 *
	 * @return the number of nodes evaluated
	 */
	//get how many nodes were evaluated by the algorithm
	int getNumberOfNodesEvaluated();
}
