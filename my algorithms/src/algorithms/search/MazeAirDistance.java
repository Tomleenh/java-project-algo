package algorithms.search;

import algorithms.mazeGenerators.Position;
import algorithms.mazeGenerators.State;

/**
 * The Class MazeAirDistance.
 * Calculates the distance air : root sum of the squares of the distances axis x, y, z.
 */
public class MazeAirDistance implements Heuristic<Position> {

	/* (non-Javadoc)
	 * @see algorithms.search.Heuristic#heuristicalDistance(algorithms.mazeGenerators.State, algorithms.mazeGenerators.State)
	 */
	@Override
	public int heuristicDistance(State<Position> currentState,State<Position> goalState) {

		int x = (int) Math.pow((currentState.getCurrentState().getX() - goalState.getCurrentState().getX()), 2);
		int y = (int) Math.pow((currentState.getCurrentState().getY() - goalState.getCurrentState().getY()), 2); 
		int z = (int) Math.pow((currentState.getCurrentState().getZ() - goalState.getCurrentState().getZ()), 2); 

		int h = (int) Math.sqrt(x+y+z);
		return h;


	}
}
