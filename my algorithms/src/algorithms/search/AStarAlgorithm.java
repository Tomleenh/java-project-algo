package algorithms.search;

import java.util.ArrayList;
import java.util.HashMap;

import algorithms.mazeGenerators.State;
import algorithms.searchable.Searchable;

/**
 * The Class AStarAlgorithm.
 * this class implements an A star Search Algorithm that uses heuristic.
 *
 * @param <T> the generic type
 */
public class AStarAlgorithm<T> extends CommonSearcher<T> {

	/** The heuristic. */
	private Heuristic<T> h;

	/**
	 * Gets the Heuristic.
	 *
	 * @return the Heuristic
	 */
	public Heuristic<T> getH() {
		return h;
	}

	/**
	 * Sets the Heuristic.
	 *
	 * @param h the new Heuristic
	 */
	public void setH(Heuristic<T> h) {
		this.h = h;
	}

	/**
	 * Instantiates a new a star algorithm.
	 *
	 * @param heuristic the heuristic
	 */
	public AStarAlgorithm(Heuristic<T> heuristic) {
		this.h = heuristic;
	}

	/* (non-Javadoc)
	 * @see algorithms.search.CommonSearcher#search(algorithms.searchable.Searchable)
	 */
	@Override
	public Solution<T> search(Searchable<T> s) {

		myHash = new HashMap<>();
		int temp = 0;
		State<T> startState;
		try{
			startState = s.getStartState();
		}catch(NullPointerException e){
			return null;
		}
		if(h!=null)
			startState.setCost(temp+h.heuristicDistance(s.getStartState(), s.getGoalState()));
		else
			startState.setCost(temp);
		openList.add(startState);
		myHash.put(s.getStartState().getCurrentState(), 0);

		//the algorithm runs till the open list is empty or we reached the goal position
		while(!openList.isEmpty())
		{
			State<T> currentState = popOpenList(); //dequeue from Priority List
			closedSet.add(currentState);

			//if we reached the goal we are doing back trace from the goal to the start through cameFrom
			if(currentState.getCurrentState().equals(s.getGoalState().getCurrentState()))
			{
				return generatePathToGoal(currentState);
			}

			//array list of possible positions to move to
			ArrayList<State<T>> successors = s.getPossibleMoves(currentState);
			for(State<T> neighbor : successors)
			{

				temp = myHash.get(currentState.getCurrentState())+neighbor.getCost();
				int oldCost = temp+1;
				if(myHash.containsKey(neighbor.getCurrentState())){
					oldCost = myHash.get(neighbor.getCurrentState());
				}

				//if the position we found isn't in the open list or the closed list meaning he's a new node we discovered
				if(!closedSet.contains(neighbor.getCurrentState()) && !openList.contains(neighbor.getCurrentState()) && (temp < oldCost))
				{
					if(h!=null)
						neighbor.setCost(temp+h.heuristicDistance(neighbor, s.getGoalState()));
					else
						neighbor.setCost(temp);
					neighbor.setCameFrom(currentState);
					openList.add(neighbor);
					myHash.put(neighbor.getCurrentState(), temp);
				}
				//if we find a neighbor that costs us less we update that neighbor
				else if (temp < oldCost){
					myHash.remove(neighbor.getCurrentState());

					openList.remove(neighbor);
					if(h!=null)
						neighbor.setCost(temp+h.heuristicDistance(neighbor, s.getGoalState()));
					else
						neighbor.setCost(temp);
					neighbor.setCameFrom(currentState);
					openList.add(neighbor);
					myHash.put(neighbor.getCurrentState(), temp);

				}
			}

		}
		return null;
	}
}

