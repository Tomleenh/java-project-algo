package algorithms.search;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.PriorityQueue;

import algorithms.mazeGenerators.State;
import algorithms.searchable.Searchable;

/**
 * The Class CommonSearcher.
 * Implements Searcher interface.
 * Implements getNumberOfNodesEvaluated method.
 *
 * @param <T> the generic type
 */
public abstract class CommonSearcher<T> implements Searcher<T> {

	/** The open list. */
	protected PriorityQueue<State<T>> openList;

	/** The closed set. */
	protected HashSet<State<T>> closedSet;

	/** The temporary hashMap. */
	protected HashMap<T, Integer> myHash;

	/** The evaluated nodes. */
	private int evaluatedNodes;

	/**
	 * Instantiates a new common searcher.
	 */
	public CommonSearcher() {
		openList = new PriorityQueue<State<T>>();
		evaluatedNodes = 0;
		closedSet = new HashSet<State<T>>();
	}

	/* (non-Javadoc)
	 * @see algorithms.search.Searcher#getNumberOfNodesEvaluated()
	 */
	@Override
	public int getNumberOfNodesEvaluated() {
		return evaluatedNodes;
	}

	/**
	 * Pop open list.
	 *
	 * @return the state
	 */
	protected State<T> popOpenList() {
		evaluatedNodes++;
		return openList.poll();
	}

	/* (non-Javadoc)
	 * @see algorithms.search.Searcher#search(algorithms.searchable.Searchable)
	 */
	public abstract Solution<T> search(Searchable<T> s);

	/**
	 * Generate path to goal.
	 *
	 * @param goal the goal State
	 * @return the solution of the problem
	 */
	public Solution<T> generatePathToGoal(State<T> goal)
	{
		ArrayList<T> solution = new ArrayList<T>();

		solution.add(goal.getCurrentState());
		State<T> p =  goal.getCameFrom();

		while(p.getCameFrom()!=null){
			solution.add(p.getCurrentState());
			p = p.getCameFrom();
		}

		solution.add(p.getCurrentState());


		ArrayList<T> backTrace = new ArrayList<T>();

		//back tracing to the solution path
		for(int i=solution.size()-1 ; i>=0 ; i--){
			backTrace.add(solution.get(i));
		}


		Solution<T> tempSol = new Solution<T>(backTrace);
		return tempSol;
	}

}
