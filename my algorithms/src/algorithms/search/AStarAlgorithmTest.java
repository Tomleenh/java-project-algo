package algorithms.search;

import static org.junit.Assert.*;

import org.junit.Test;

import algorithms.demo.Maze3dSearchableAdapter;
import algorithms.mazeGenerators.Maze3d;
import algorithms.mazeGenerators.MyMaze3dGenerator;
import algorithms.mazeGenerators.Position;

/**
 * The Class AStarAlgorithmTest.
 */
public class AStarAlgorithmTest {

	/**
	 * Test get null Heuristic.
	 */
	@Test
	public void testGetNullH() {
		Maze3d maze3D = new MyMaze3dGenerator().generate(3, 3, 3);
		Maze3dSearchableAdapter mazeSearch = new Maze3dSearchableAdapter(maze3D);
		Searcher<Position> searcher= new AStarAlgorithm<Position>(null);
		Searcher<Position> searcher2= new BFSAlgorithm<Position>();
		Solution<Position> sol = new Solution<Position>();
		Solution<Position> solBFS = new Solution<Position>();

		sol = searcher.search(mazeSearch);
		solBFS = searcher2.search(mazeSearch);

		assertEquals(sol, solBFS);

	}

	/**
	 * Test get null maze3d searchable adapter.
	 */
	@Test
	public void testGetNullMaze3dSearchableAdapter() {

		Searcher<Position> searcher= new AStarAlgorithm<Position>(new MazeAirDistance());		
		Solution<Position> sol = new Solution<Position>();	
		sol = searcher.search(null);

		assertNull(sol);
	}

	/**
	 * Test set null maze3d.
	 */
	@Test
	public void testSetNullMaze3d() {

		Maze3dSearchableAdapter mazeSearch = new Maze3dSearchableAdapter(null);
		Searcher<Position> searcher= new AStarAlgorithm<Position>(new MazeAirDistance());		
		Solution<Position> sol = new Solution<Position>();	
		sol = searcher.search(mazeSearch);

		assertNull(sol);
	}

}
