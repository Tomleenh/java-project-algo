package algorithms.mazeGenerators;

import java.io.Serializable;

/**
 * The Class Position.
 */
@SuppressWarnings("serial")
public class Position implements Serializable{

	/** The x - row, y - height and z - column. */
	protected int x, y, z;

	/**
	 * Instantiates a new position.
	 *
	 * @param x the row
	 * @param y the height
	 * @param z the column
	 */
	public Position( int x, int y, int z){
		this.x = x;
		this.y = y;
		this.z = z;
	}

	/**
	 * prints the object as {row,height,column}.
	 *
	 * @return the string
	 */
	public String toString() {
		return "{" + this.getX() + "," + this.getY() + "," + this.getZ() + "}";
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + x;
		result = prime * result + y;
		result = prime * result + z;
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Position other = (Position) obj;
		if (x != other.x)
			return false;
		if (y != other.y)
			return false;
		if (z != other.z)
			return false;
		return true;
	}

	/**
	 * Gets the x (row).
	 *
	 * @return the x (row)
	 */
	public int getX() {
		return x;
	}

	/**
	 * Sets the x (row).
	 *
	 * @param x the new x (row)
	 */
	public void setX(int x) {
		this.x = x;
	}

	/**
	 * Gets the y (height).
	 *
	 * @return the y (height)
	 */
	public int getY() {
		return y;
	}

	/**
	 * Sets the y (height).
	 *
	 * @param y the new y (height)
	 */
	public void setY(int y) {
		this.y = y;
	}

	/**
	 * Gets the z (column).
	 *
	 * @return the z (column)
	 */
	public int getZ() {
		return z;
	}

	/**
	 * Sets the z (column).
	 *
	 * @param z the new z (column)
	 */
	public void setZ(int z) {
		this.z = z;
	}

	/**
	 * Split.
	 * 
	 * @return int[] the position params.
	 */
	public int[] split() {
		String[] tempString = this.toString().replace("{", "").replace("}", "").split(",");
		int[] tempIntArray = new int[tempString.length];
		for(int i=0;i<tempString.length;i++){
			tempIntArray[i] = Integer.parseInt(tempString[i]);
		}
		return tempIntArray;
	}
}

