package algorithms.mazeGenerators;

import java.util.Random;

/**
 * The Class SimpleMaze3dGenerator.
 * extends Maze3dGeneratorAbstract.
 * creates a random maze3d
 */
public class SimpleMaze3dGenerator extends AbstractMaze3dGenerator {

	/* (non-Javadoc)
	 * @see algorithms.mazeGenerators.AbstractMaze3dGenerator#generate(int, int, int)
	 */
	@Override
	public Maze3d generate(int x, int y, int z) {
		int[][][] tempMaze = new int[x][y][z];
		Maze3d mg = new Maze3d(x,y,z);
		mg.setStartPosition(new Position(0, 0, 0));
		mg.setGoalPosition(new Position(0, y-1, 0));
		int height = 0; //height = Y
		int row=0,column=0; //row = X , column = Z
		Random rand = new Random();

		//creating the almost random game board - 0=we can move, 1=wall
		while(height<y){ 
			for(row=0;row<x;row++){
				for(column=0;column<z;column++){
					if(row == 0 && column == 0)
					{
						tempMaze[row][height][column] = 0;
					}
					else{
						tempMaze[row][height][column] = rand.nextInt(2);
					}
				}
			}
			height++;
		}
		mg.setMaze(tempMaze);
		return mg;
	}
}

