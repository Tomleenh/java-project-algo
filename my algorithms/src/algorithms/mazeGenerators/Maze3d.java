package algorithms.mazeGenerators;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * The Class Maze3d.
 */
public class Maze3d implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 6749758573633639278L;

	/** 3D array - the maze. */
	protected int[][][] maze;

	//three data members to express the row, column and height of the maze. 
	/** The row. */
	private int row;

	/** The column. */
	private int column;

	/** The height. */
	private int height;

	/** The start and goal positions. */
	protected Position startPosition,goalPosition;

	/**
	 * Gets the maze.
	 *
	 * @return the maze
	 */
	public int[][][] getMaze() {
		return maze;
	}

	/**
	 * Sets the maze.
	 *
	 * @param newMaze the new maze
	 */
	public void setMaze(int[][][] newMaze) { //setter that gets an 3D newMaze and sets the game board.
		this.maze = newMaze.clone();
	}

	/**
	 * Gets the start position.
	 *
	 * @return the start position
	 */
	public Position getStartPosition() {
		return startPosition;
	}

	/**
	 * Sets the start position.
	 *
	 * @param startPosition
	 *            the new start position
	 */
	public void setStartPosition(Position startPosition) {
		this.startPosition = startPosition;
	}

	/**
	 * Gets the goal position.
	 *
	 * @return the goal position
	 */
	public Position getGoalPosition() {
		return goalPosition;
	}

	/**
	 * Sets the goal position.
	 *
	 * @param goalPosition the new goal position
	 */
	public void setGoalPosition(Position goalPosition) {
		this.goalPosition = goalPosition;
	}

	/**
	 * Gets the row.
	 *
	 * @return the row
	 */
	public int getRow() {
		return row;
	}

	/**
	 * Sets the row.
	 *
	 * @param row the new row
	 */
	public void setRow(int row) {
		this.row = row;
	}

	/**
	 * Gets the column.
	 *
	 * @return the column
	 */
	public int getColumn() {
		return column;
	}

	/**
	 * Sets the column.
	 *
	 * @param column the new column
	 */
	public void setColumn(int column) {
		this.column = column;
	}

	/**
	 * Gets the height.
	 *
	 * @return the height
	 */
	public int getHeight() {
		return height;
	}

	/**
	 * Sets the height.
	 *
	 * @param height the new height
	 */
	public void setHeight(int height) {
		this.height = height;
	}

	/**
	 * Instantiates a new maze3d.
	 *
	 * @param x the row
	 * @param y the height
	 * @param z the column
	 */
	public Maze3d(int x, int y, int z) {
		this.maze = new int[x][y][z]; //initialize a new maze
		this.row = x;
		this.height = y;
		this.column = z;
		this.startPosition = null;
		this.goalPosition = null;
	}

	/**
	 * Instantiates a new maze3d from byte array.
	 *
	 * @param byteArray the byte array
	 */
	public Maze3d(byte[] byteArray) {
		this.startPosition = new Position(ByteBuffer.wrap(Arrays.copyOfRange(byteArray, 0, 4)).getInt(),ByteBuffer.wrap(Arrays.copyOfRange(byteArray, 4, 8)).getInt(),ByteBuffer.wrap(Arrays.copyOfRange(byteArray, 8, 12)).getInt());
		this.goalPosition = new Position((ByteBuffer.wrap(Arrays.copyOfRange(byteArray, 12, 16)).getInt()),ByteBuffer.wrap(Arrays.copyOfRange(byteArray, 16, 20)).getInt(),ByteBuffer.wrap(Arrays.copyOfRange(byteArray, 20, 24)).getInt());
		this.row = ByteBuffer.wrap(Arrays.copyOfRange(byteArray, 24, 28)).getInt();
		this.height = ByteBuffer.wrap(Arrays.copyOfRange(byteArray, 28, 32)).getInt();
		this.column = ByteBuffer.wrap(Arrays.copyOfRange(byteArray, 32, 36)).getInt();
		this.maze = new int[this.row][this.height][this.column];
		int i = 36;
		int y=0;
		while(y<this.height){
			for(int x=0;x<row;x++){
				for(int z=0;z<column;z++){
					this.maze[x][y][z] = ByteBuffer.wrap(Arrays.copyOfRange(byteArray, i, (i+4))).getInt();	
					i+=4;
				}
			}
			y++;
		}
	}

	/**
	 * Gets the possible moves.
	 *
	 * @param p the position
	 * @return array list of the possible moves
	 */
	public ArrayList<Position> getPossibleMoves(Position p) {
		ArrayList<Position> possMoves = new ArrayList<Position>();
		int xn = p.getX()-1;
		int xp = p.getX()+1;
		int yn = p.getY()-1;
		int yp = p.getY()+1;
		int zn = p.getZ()-1;
		int zp = p.getZ()+1;

		if (xn>=0 && (maze[xn][p.getY()][p.getZ()] == 0)){
			possMoves.add(new Position(xn,p.getY(),p.getZ()));
		}

		if (xp<row && (maze[xp][p.getY()][p.getZ()] == 0)){
			possMoves.add(new Position(xp,p.getY(),p.getZ()));
		}

		if (yn>=0 && (maze[p.getX()][yn][p.getZ()] == 0)){
			possMoves.add(new Position(p.getX(),yn,p.getZ()));
		}
		if (yp<height && (maze[p.getX()][yp][p.getZ()] == 0)){
			possMoves.add(new Position(p.getX(),yp,p.getZ()));
		}

		if (zn>=0 && (maze[p.getX()][p.getY()][zn] == 0)){
			possMoves.add(new Position(p.getX(),p.getY(),zn));
		}
		if (zp<column && (maze[p.getX()][p.getY()][zp] == 0)){
			possMoves.add(new Position(p.getX(),p.getY(),zp));

		}

		return possMoves;
	}

	/**
	 * Prints the maze.
	 */
	public void printMaze() {
		for(int i=0;i<this.getHeight();i++)
		{
			if(i!=0)
				System.out.println();
			for(int j=0;j<this.getRow();j++)
			{
				System.out.println();
				for(int g=0;g<this.getColumn();g++)
				{
					System.out.print(maze[j][i][g]);
				}
			}
		}
	}

	/**
	 * converting the maze including start and goal positions, row, height, column and the maze from int[] to byte[].
	 *
	 * @return the byte[]
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public byte[] toByteArray() throws IOException{
		ByteArrayOutputStream byteArrayOut = new ByteArrayOutputStream();
		DataOutputStream dataOut = new DataOutputStream(byteArrayOut);
		int x=0,y=0,z=0;

		int[] startPositionArray = this.getStartPosition().split();
		int[] goalPositionArray = this.getGoalPosition().split();
		int[][][] tempMaze = this.getMaze();

		for(int i=0;i<startPositionArray.length;i++){
			dataOut.write(ByteBuffer.allocate(4).putInt(startPositionArray[i]).array());
			dataOut.flush();
		}

		for(int i=0;i<goalPositionArray.length;i++){
			dataOut.write(ByteBuffer.allocate(4).putInt(goalPositionArray[i]).array());
			dataOut.flush();
		}

		dataOut.write(ByteBuffer.allocate(4).putInt(row).array());
		dataOut.flush();
		dataOut.write(ByteBuffer.allocate(4).putInt(height).array());
		dataOut.flush();
		dataOut.write(ByteBuffer.allocate(4).putInt(column).array());
		dataOut.flush();

		while(y<this.getHeight()){
			for(x=0;x<this.getRow();x++){
				for(z=0;z<this.getColumn();z++){
					int i = tempMaze[x][y][z];
					dataOut.write(ByteBuffer.allocate(4).putInt(i).array());
					dataOut.flush();
				}
			}
			y++;
		}
		return byteArrayOut.toByteArray();
	}

	/**
	 * Gets the cross section by Xx.
	 *
	 * @param x the row
	 * @return 2d array of cross section by x
	 */
	public int[][] getCrossSectionByX(int x) {
		if(x>=this.getRow() || x < 0)
		{
			throw new IndexOutOfBoundsException("your number is not within the limits of the maze borders");
		}
		int[][] tempMatrix = new int[this.getHeight()][this.getColumn()];
		for(int tempHeight = 0;tempHeight<this.getHeight();tempHeight++)
		{
			for(int tempColumn = 0;tempColumn<this.getColumn();tempColumn++)
			{
				tempMatrix[tempHeight][tempColumn] = this.maze[x][tempHeight][tempColumn];
			}
		}
		return tempMatrix;
	}

	/**
	 * Gets the cross section by Y.
	 *
	 * @param y the height
	 * @return 2d array of cross section by y
	 */
	public int[][] getCrossSectionByY(int y) {
		if(y>=this.getHeight() || y < 0)
		{
			throw new IndexOutOfBoundsException("your number is not within the limits of the maze borders");
		}
		int[][] tempMatrix = new int[this.getRow()][this.getColumn()];
		for(int tempRow = 0;tempRow<this.getRow();tempRow++)
		{
			for(int tempColumn = 0;tempColumn<this.getColumn();tempColumn++)
			{
				tempMatrix[tempRow][tempColumn] = this.maze[tempRow][y][tempColumn];
			}
		}
		return tempMatrix;
	}

	/**
	 * Gets the cross section by Z.
	 *
	 * @param z the column
	 * @return 2d array of cross section by z
	 */
	public int[][] getCrossSectionByZ(int z) {
		if(z>=this.getColumn() || z < 0)
		{
			throw new IndexOutOfBoundsException("your number is not within the limits of the maze borders");
		}
		int[][] tempMatrix = new int[this.getRow()][this.getHeight()];
		for(int tempRow= 0;tempRow<this.getRow();tempRow++)
		{
			for(int tempHeight = 0;tempHeight<this.getColumn();tempHeight++)
			{
				tempMatrix[tempRow][tempHeight] = this.maze[tempRow][tempHeight][z];
			}
		}
		return tempMatrix;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Maze3d other = (Maze3d) obj;
		if (column != other.column)
			return false;
		if (goalPosition == null) {
			if (other.goalPosition != null)
				return false;
		} else if (!goalPosition.equals(other.goalPosition))
			return false;
		if (height != other.height)
			return false;
		if (!Arrays.deepEquals(maze, other.maze))
			return false;
		if (row != other.row)
			return false;
		if (startPosition == null) {
			if (other.startPosition != null)
				return false;
		} else if (!startPosition.equals(other.startPosition))
			return false;
		return true;
	}
}
