package algorithms.mazeGenerators;

/**
 * The Class AbstractMaze3dGenerator.
 */
public abstract class AbstractMaze3dGenerator implements Maze3dGenerator {

	/**
	 * Method to generate the maze.
	 *
	 * @param x the row
	 * @param y the height
	 * @param z the column
	 * @return Maze3d object with generated maze
	 */
	public abstract Maze3d generate(int x, int y, int z);

	/**
	 * This is the measure Algorithm running Time method which measure the Algorithm of making 3D maze running Time
	 * by get the start time, Creates the maze and get the time again.
	 * now calculates the end time - start time and this is the Algorithm running Time
	 *
	 * @param x the row
	 * @param y the height
	 * @param z the column
	 * @return the time it took to generate the maze as a long parameter number
	 */
	public String measureAlgorithmTime(int x, int y, int z) {
		long startTime,endTime;

		startTime = System.currentTimeMillis();
		generate(x, y, z);
		endTime=System.currentTimeMillis();
		return "Algorithm Running Time in Milliseconds: " + String.valueOf(endTime-startTime);
	}
}
