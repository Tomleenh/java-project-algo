package algorithms.mazeGenerators;

import java.io.Serializable;

/**
 * The Class State.
 *
 * @param <T> the generic type
 */
@SuppressWarnings({ "rawtypes", "serial" })
public class State<T> implements Comparable<State>, Serializable{

	/** The current state. */
	private T currentState;

	/** The came from. */
	private State<T> cameFrom;

	/** The cost. */
	private int cost;

	/**
	 * Instantiates a new state.
	 *
	 * @param currentState the current state
	 * @param cameFrom the came from
	 * @param cost the cost
	 */
	public State(T currentState, State<T> cameFrom,  int cost) {
		this.currentState = currentState;
		this.cost = cost;
		this.cameFrom = cameFrom;
	}

	/**
	 * Gets the current state.
	 *
	 * @return the current state
	 */
	public T getCurrentState() {
		return currentState;
	}

	/**
	 * Sets the current state.
	 *
	 * @param currentState the new current state
	 */
	public void setCurrentState(T currentState) {
		this.currentState = currentState;
	}

	/**
	 * Gets the came from State.
	 *
	 * @return the came from state
	 */
	public State<T> getCameFrom() {
		return cameFrom;
	}

	/**
	 * Sets the came from State.
	 *
	 * @param cameFrom the new came from
	 */
	public void setCameFrom(State<T> cameFrom) {
		this.cameFrom = cameFrom;
	}

	/**
	 * Gets the cost.
	 *
	 * @return the cost
	 */
	public int getCost() {
		return cost;
	}

	/**
	 * Sets the cost.
	 *
	 * @param cost the new cost
	 */
	public void setCost(int cost) {
		this.cost = cost;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		return this.getCurrentState().toString();
	}

	/* (non-Javadoc)
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(State o) {
		return (int) (this.getCost()-o.getCost());
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((currentState == null) ? 0 : currentState.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		State other = (State) obj;
		if (currentState == null) {
			if (other.currentState != null)
				return false;
		} else if (!currentState.equals(other.currentState))
			return false;
		return true;
	}


}

