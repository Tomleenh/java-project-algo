package algorithms.mazeGenerators;

/**
 * The Interface Maze3dGenerator.
 */
public abstract interface Maze3dGenerator {

	/**
	 * Generate.
	 *
	 * @param x the row
	 * @param y the height
	 * @param z the column
	 * @return generated maze3d
	 */
	Maze3d generate(int x,int y, int z);

	/**
	 * Measure algorithm time.
	 *
	 * @param x the row
	 * @param y the height
	 * @param z the column
	 * @return the time it took to generate the maze in mili seconds as string
	 */
	String measureAlgorithmTime(int x,int y, int z);
}
