package algorithms.searchable;

import java.util.ArrayList;

import algorithms.mazeGenerators.State;

/**
 * The Interface Searchable.
 *
 * @param <T> the generic type
 */
public interface Searchable<T> {

	/**
	 * Gets the start State.
	 *
	 * @return the start State
	 */
	State<T> getStartState();

	/**
	 * Gets the goal State.
	 *
	 * @return the goal State
	 */
	State<T> getGoalState();

	/**
	 * Gets the possible moves.
	 *
	 * @param currentState the current State
	 * @return array list of the possible moves
	 */
	ArrayList<State<T>> getPossibleMoves(State<T> currentState);
}
