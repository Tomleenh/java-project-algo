package io;

import java.io.IOException;
import java.io.InputStream;

/**
 * The Class MyDecompressorInputStream.
 * Decompress the maze
 */
public class MyDecompressorInputStream extends InputStream {

	/** The in. */
	private InputStream in;

	/**
	 * Instantiates a new my decompressor input stream.
	 *
	 * @param input the input
	 */
	public MyDecompressorInputStream(InputStream input) {
		this.in = input;
	}

	/* (non-Javadoc)
	 * @see java.io.InputStream#read()
	 */
	@Override
	public int read() throws IOException {
		return in.read();
	}

	/* (non-Javadoc)
	 * @see java.io.InputStream#read(byte[])
	 */
	public int read(byte[] byteArray) throws IOException{
		int i=0,counter=0,number=0;

		while(i<byteArray.length && ((number=in.read()) != -1) && ((counter=in.read()) != -1)){
			for(int k=0;k<counter;k++,i++){
				byteArray[i] = (byte)number;
			}
		}
		return byteArray.length;
	}
}
