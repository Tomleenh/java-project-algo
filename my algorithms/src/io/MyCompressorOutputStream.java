package io;

import java.io.IOException;
import java.io.OutputStream;

/**
 * The Class MyCompressorOutputStream.
 * Compress the maze
 */
public class MyCompressorOutputStream extends OutputStream {

	/** The out. */
	private OutputStream out;

	/**
	 * Instantiates a new my compressor output stream.
	 *
	 * @param output the output
	 */
	public MyCompressorOutputStream(OutputStream output) {
		this.out = output;
	}

	/* (non-Javadoc)
	 * @see java.io.OutputStream#write(int)
	 */
	@Override
	public void write(int b) throws IOException {
		out.write(b);
	}

	/* (non-Javadoc)
	 * @see java.io.OutputStream#write(byte[])
	 */
	public void write(byte[] byteArray) throws IOException{
		int lastInt;
		int counter=0,i=0;

		while(i<byteArray.length){
			lastInt=byteArray[i];
			while((i!=byteArray.length) && lastInt==byteArray[i]){
				counter++;
				i++;
			}
			this.write(lastInt);
			this.write(counter);
			counter=0;
		}
	}
}
